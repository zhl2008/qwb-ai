#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
@name challenge module for RHG-AI
@author haozigege@Lancet
@version v0.1
@time 2018.9

This module is used to interact with the interface, and download/provide the 
challenge information for the fuzzing module

'''

import os
import sys
import config
import json
import requests
import Queue
from random import randint
import threading
import time
import stack
import fmt
from my_file import my_file
from pwn import *
import log


reload(sys)  
sys.setdefaultencoding('utf8')

class challenge():

	def __init__(self):
		self.user = config.user
		self.pwd = config.pwd
		self.token = config.token
		self.interface = config.interface
		self.status_url = config.status_url
		self.flag_url = config.flag_url
		self.heartbeat_url = config.heartbeat_url
		self.headers = {"Content-type":"application/json, charset:utf-8, Accept: text/plain"}
		self.down_path = config.down_path
		self.exploit_queue = Queue.Queue(1000)
		self.download_queue = Queue.Queue(1000)
		self.thread_array = []
		self.thread_watch_span = config.thread_watch_span
		self.challenge = {}
		self.all_challenges = []

		self.shellcode = []
		self.simple_stack = []
		self.fmt = []
		self.heap = []
		self.unknown = []



	def challenge_status(self,challenge_id):
		'''
			get status of a specific challenge
		'''
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'

		if os.path.exists(binary_dir) and os.path.exists(binary_log):

			log = json.loads(open(binary_log).read())
			return log['status']

		else:
			return 'not_start_yet'


	def heart_beat(self):
		real_url = self.interface + self.heartbeat_url
		while True:
			try:
				data = '{"token":"%s","proc":{"cpu_usage":0.0,"mem_usage":0.0}}' % self.token
				r = requests.post(real_url,headers=self.headers,verify=False,data=data)
			except Exception,e:
				log.error(str(e))
			time.sleep(60)



	def get_status(self):
		'''
			get status(challenges and score) from the interface
		'''

		real_url = self.interface + self.status_url
		

		data = '{"token":"%s"}' % self.token
		
		# avoid the redundant id
		exist_ids = []


		while True:

			test = 1

			new_challenges = []
			# loop until there is no error
			while test:
				try:
					r = requests.post(real_url,headers=self.headers,verify=False,data=data)
					#print r.content
					self.status = json.loads(r.content)
					log.context(r.content)
					if self.status['status'] != 'ok':
						time.sleep(2)
						log.warning('game not start!')
						continue
					test = 0
				except Exception,e:
					test = 1
					log.error(str(e))
			

			# prepare the download path
			if not os.path.exists(self.down_path):
				os.system('mkdir -p %s' %self.down_path)

			
			# test if it's a new batch challenges
			for challenge in self.status['data']:
				if challenge['id'] in exist_ids:
					continue
				else:
					exist_ids.append(challenge['id'])
					self.all_challenges.append(challenge)
					new_challenges.append(challenge)

			if not new_challenges:
				log.warning('no new challenges')
			else:
				log.info(str(new_challenges))
			# put all the challenge to download queue
			for challenge in new_challenges:
				self.download_queue.put(challenge)
				# print challenge
				# exist_ids.append(challenge['id'])

				tmp = {}
				tmp['path'] = self.down_path + '/' + str(challenge['id']) + '/bin' + str(challenge['id'])
				tmp['file_id'] = challenge['id']
				tmp['flag_path'] = 'abandon'
				tmp['server_ip'] = challenge['access'][0]['ip']
				tmp['server_port'] = challenge['access'][0]['port']

				self.challenge[challenge['id']] = tmp

			# To change the status of running to not_start_yet
			for challenge in new_challenges:
				challenge_id = challenge['id']
				info_path = '%s/%s/info' % (self.down_path,str(challenge_id))
				if not os.path.exists(info_path):
					continue
				status = json.loads(open(info_path).read())
				if status['status'] == 'running':
					status['status'] == 'not_start_yet'
					open(info_path,'w').write(json.dumps(status))
					log.warning('changing status to not_start_yet in %s' % str(challenge_id))

			# wait for another batch
			time.sleep(30)



	def before_download(self,url):
		while True:
			try:
				r = requests.head(url)
				if r.status_code == 200:
					return True
				else:
					log.error('status code error %s' %str(r.status_code))
					time.sleep(2)
			except Exception,e:
				log.error(str(e))


	def download_challenge(self):

		'''
			download the challenges

		'''

		while True:

			if self.download_queue.empty():
				# log.warning('empty download queue')
				time.sleep(3)
				continue

			challenge = self.download_queue.get()

			challengeID = challenge['id']
			binaryUrl = challenge['binary_url']

			os.system('mkdir -p %s/%s' %(self.down_path,challengeID))

			self.before_download(binaryUrl)
			os.system('wget %s --no-check-certificate -O %s/%s/bin%s 1>/dev/null 2>&1' %(binaryUrl,self.down_path,challengeID,challengeID))
			log.success('binary downloaded: %s' %challengeID)

			# add the execute priv
			os.system('chmod +x %s/%s/bin%s'%(self.down_path,challengeID,challengeID))

			# if a challenge is a simple stack challenge or shellcode, do it firstly
			status = self.challenge_status(challengeID)

			if status == 'abandon' or status == 'done':
				continue

			self.exploit_queue.put(challengeID)



	def check_status(self,challenge_id):
		'''
			judge a status of a challenge
		'''	
		status = self.challenge_status(challenge_id)
		#log.success(str(challenge_id)+" is "+status)
		if status == 'abandon' or status == 'done' or status == 'running':
			return False
		return True


	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'

			if not self.exploit_queue.empty():
				# print self.exploit_queue.qsize()
				all_exp_challenges = str(list(self.exploit_queue.queue))
			else:
				all_exp_challenges = 'empty'
			res += '[+] existing exp queue: %s'%all_exp_challenges

			log.context(res)


	def exploit_call(self):

		while True:

			# if the exploit queue is empty, wait for 3 seconds
			if self.exploit_queue.empty():
				time.sleep(3)
				continue

			else:
				challenge_id = self.exploit_queue.get()
				log.info('start exploit %s' %str(challenge_id))
				self.attack(challenge_id)
				log.info('exploit for %s done' %str(challenge_id))



	def get_challenge_status(self,challenge_id):
		'''
			get the status of a challenge
		'''
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'

		return json.loads(open(binary_log,'r').read())

	def attack_stack(self, challenge_id):

		status = self.get_challenge_status(challenge_id)
		if status['flag']:
			return status
		if status['stack_mode'] == 'stage0':
			log.success(str(challenge_id)+"=====>>>>stage0")
			status = stack.stack_api(self.challenge[challenge_id],0, 0x80, config.default_timeout)
		else:
			status['status'] = "abandon"
		return status


	def attack(self,challenge_id):
		'''
			this is an example of what the attack method should be.

			the info file for a challenge:
			id      1
			status  not_start_yet/done/abandon/suspend/running
			flag    flag{81b9c5e90ba6f16fc8a674337fb91623}
			start_time    1537204727
			end_time    1537204737

		'''

		# prepare
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'
		if os.path.exists(binary_log):
			status = self.get_challenge_status(challenge_id)
			status['status'] = 'running'
			open(binary_log,'w').write(json.dumps(status))
		else:
			status = {}
			status['id'] = challenge_id
			status['status'] = 'running'
			status['flag'] = ''
			status['start_time'] = str(int(time.time()))
			status['end_time'] = ''
			status['stack_mode'] = 'stage0'
			open(binary_log,'w').write(json.dumps(status))

		log.success(str(challenge_id)+" started")

		# exploit and get flag
		# In the new version flag is not required, so set the flag to 1 if crash successfully


		status = self.attack_stack(challenge_id)
		if status['flag']:
			flag = status['flag']
			# self.flag_submit(flag)
			log.success("Get flag!!!=++>>>>>"+str(challenge_id)+" "+ "crashed!")
		# submit flag and done
		#self.flag_submit(flag)
		status['end_time'] = str(int(time.time()))
		#status['flag'] = flag
		#status['status'] = 'done'
		open(binary_log,'w').write(json.dumps(status))



if __name__ == '__main__':
	c = challenge()

	t = threading.Thread(target=c.get_status,name='get_status_thread')
	c.thread_array.append(t)

	for i in range(config.down_thread):
		t = threading.Thread(target=c.download_challenge,name='download_thread_%d'%(i+1))	
		log.info('one thread for downloading has been created!')
		c.thread_array.append(t)

	for i in range(config.exploit_thread):
		t = threading.Thread(target=c.exploit_call,name='exploit_thread_%d'%(i+1))	
		log.info('one thread for exploiting has been created!')
		c.thread_array.append(t)


	t = threading.Thread(target=c.heart_beat,name='heart_beat_thread')
	c.thread_array.append(t)

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	c.thread_array.append(t)


	for t in c.thread_array:
		t.setDaemon(True)
		t.start()

	try:
		while True:
			time.sleep(3)
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()

