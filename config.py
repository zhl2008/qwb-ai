#!/usr/bin/env python

'''
	configuration for the game

'''

# username for the challenge interface
user = ''

# password for the challenge interface
pwd = ''

# team token
token = 'bf6332cc57e6411a97e04fc10b8db564'

# url for the interface
#interface = 'https://rhg.ichunqiu.com/rhg'
interface = 'http://111.198.29.45:9555'

# interface for getting the status
status_url = '/challenges'

# interface for submitting the flag
flag_url = '/flag'

# interface for heartbeat
heartbeat_url = '/heartbeat'

# interface for scoreboard
scoreboard_url = '/scoreboard'

# binary download path
down_path = './tmp'

# log file
log_file = 'my_log'

# whether write logs to file
log_to_file = 1

# download challenge thread num
down_thread = 3

# exploit thread num
exploit_thread = 2

# thread watch time span
thread_watch_span = 30

# stack
ROPgadget_path = "/usr/local/bin/ROPgadget"
#ROPgadget_path = "/home/angr/.virtualenvs/angr/bin/ROPgadget"
#flag_path = "/home/angr/cncert/flag"
#binary_path = "/home/angr/cncert/bin"

# fuzz
core_num = 1
shellphuzz_path = "/usr/local/bin/shellphuzz"
#shellphuzz_path = "/home/angr/.virtualenvs/angr/bin/shellphuzz"
#fuzzdir_path = "/root/qwb-ai/fuzzdir"
fuzzdir_path = "/home/angr/fuzzdir"
extral_cmd = " -c "+str(core_num)
default_timeout = 7200




