#!/bin/bash


# start ssh
service ssh start

# rm tmp file
rm -r /home/angr/fuzzdir
mkdir -p  /home/angr/fuzzdir
chmod 777 -R /home/angr/fuzzdir

# run the whole program
/root/run/watchdog.sh
